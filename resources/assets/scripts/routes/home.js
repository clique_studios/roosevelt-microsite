import Swiper from "swiper/dist/js/swiper.js";
import {TweenLite} from "gsap";
import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js';

export default {
    init() {
        //Platform Swiper
        var platformsSwiper = new Swiper('#solution-platforms .swiper-container', {
            speed: 1000,
            slidesPerView: 2.79,
            spaceBetween: 30,
            navigation: {
                nextEl: '#solution-platforms .swiper-button-next',
                prevEl: '#solution-platforms .swiper-button-prev',
            },
            a11y: true,
            breakpoints: {
                450: {
                    slidesPerView: 1.15,
                    spaceBetween: 35,
                },
                768: {
                    slidesPerView: 1.75,
                    spaceBetween: 35,
                },
                960: {
                    slidesPerView: 1.85,
                    spaceBetween: 35,
                },
                1200: {
                    slidesPerView: 2.15,
                    spaceBetween: 30,
                }
            }
        });


        let windowHeight = $(window).innerHeight();
        let screenHeight = $('.platform-showcase-cnt img').innerHeight();

        var topController = new ScrollMagic.Controller();

        new ScrollMagic.Scene({triggerElement: "#main", triggerHook: 0, duration: windowHeight, offset: 0, rotation:0.001,})
            .setTween(".main__header__bg", {top: "3%"})
            .addTo(topController);

        // new ScrollMagic.Scene({triggerElement: "#main", triggerHook: 0, duration: windowHeight, offset: 0})
        //   .setTween(".main__header__accent__img", {transform: "translateX(-100px)"})
        //   .addTo(topController);


        var showcaseController = new ScrollMagic.Controller();

        new ScrollMagic.Scene({
            triggerElement: "#solution-platforms",
            duration: windowHeight + screenHeight,
            offset: 300
        })
            .setTween(".showcase__bg", {transform: "translateY(-150px)"})
            .addTo(showcaseController);

        new ScrollMagic.Scene({triggerElement: "#showcase", duration: windowHeight + screenHeight, offset: -300, rotation:0.001,})
            .setTween(".showcase__accent-inner", {transform: "translateY(-300px)"})
            .addTo(showcaseController);

        new ScrollMagic.Scene({triggerElement: "#showcase", duration: windowHeight + screenHeight, offset: -300, rotation:0.001,})
            .setTween(".showcase__accent__img", {transform: "translateX(150px)"})
            .addTo(showcaseController);


        var laptopController = new ScrollMagic.Controller();

        new ScrollMagic.Scene({
            triggerElement: "#showcase",
            triggerHook: 0.2,
            offset: 350,
            duration: screenHeight,
            pushFollowers: false
        })
            .setPin("#showcase-pin")
            .addTo(laptopController);

        new ScrollMagic.Scene({triggerElement: "#showcase", triggerHook: 0.2, offset: 350, duration: screenHeight, rotation:0.001,})
            .setTween('.platform-showcase-cnt img', {top: "-163%"})
            .addTo(laptopController);

        var laptopController = new ScrollMagic.Controller();


        //Other Platform Swiper
        var other_platformsSwiper = new Swiper('#other-platforms .swiper-container', {
            speed: 1000,
            slidesPerView: 2.79,
            spaceBetween: 30,
            navigation: {
                nextEl: '#other-platforms .swiper-button-next',
                prevEl: '#other-platforms .swiper-button-prev',
            },
            a11y: true,
            breakpoints: {
                450: {
                    slidesPerView: 1.15,
                    spaceBetween: 35,
                },
                768: {
                    slidesPerView: 1.75,
                    spaceBetween: 35,
                },
                960: {
                    slidesPerView: 1.85,
                    spaceBetween: 35,
                },
                1200: {
                    slidesPerView: 2.15,
                    spaceBetween: 30,
                }
            }
        });

    },
    finalize() {
        // JavaScript to be fired on the home page, after the init JS
        let bannerImage = $(".main__header__accent__img__animate__wrap");
        let bannerImageWaypointTop = new Waypoint({
            element: bannerImage[0],
            handler: function (direction) {
                if (direction === "down") {
                    bannerImage.find("img").addClass("show");
                }
            },
            offset: "-25%",
        });

        let bannerImageWaypointBottom = new Waypoint({
            element: bannerImage[0],
            handler: function (direction) {
                if (direction === "down") {
                    bannerImage.find("img").removeClass("show");
                }
            },
            offset: "-120%",
        });

        let bannerImageWaypointUp = new Waypoint({
            element: bannerImage[0],
            handler: function (direction) {
                if (direction === "up") {
                    bannerImage.find("img").addClass("show");
                }
            },
            offset: "-100%",
        });

        let iconsSectionWaypoint = new Waypoint({
            element: $("#icons-section .icons-cnt")[0],
            handler: function () {
                $("#icons-section .icons-cnt").addClass("show");
            },
            offset: "50%"
        });

        $('#icons-section .icons-cnt .icons-row').each(function(e){
            var reveal = $(this);
            var waypoint_reveal = new Waypoint({
                element: reveal[0],
                handler: function(direction) {
                    if (direction === 'down'){
                        reveal.addClass('on');
                    }
                },
                offset: '90%'
            });
        });
        if (window.innerWidth > 767){
            $('#icons-section .icons-cnt .icons-row').each(function(){
                $(this).children('.icon').each(function(index){
                    $(this).css({'transition-delay' : 0.2*(1+index) + 's'});
                });
            });
        }

        let solutionWaypoint = new Waypoint({
            element: $("#solution-platforms")[0],
            handler: function () {
                $("#solution-platforms").addClass("show");
            },
            offset: "50%"
        });

        let other_solutionWaypoint = new Waypoint({
            element: $("#other-platforms")[0],
            handler: function () {
                $("#other-platforms").addClass("show");
            },
            offset: "-100%"
        });

        let contactWaypoint = new Waypoint({
            element: $(".contact__img")[0],
            handler: function () {
                $(".contact__img").addClass("show");
            },
            offset: "50%"
        });

        // Solution/Platform button
        function scrollToAnchor(aid) {
            var aTag = $(aid);
            var focus = true;
            var offset = parseFloat($("#showcase").css("padding-top")) / 2;
            $('html,body').animate({scrollTop: aTag.offset().top + offset}, 'slow');
            var first_child = $($("#showcase").children()[0]);
            var tag = first_child.prop("tagName").toLowerCase();
            if (tag !== "a" && tag !== "button" && tag !== "input" && tag !== "textarea") {
                first_child.attr("tabIndex", -1).focus();
            } else {
                first_child.focus();
            }
        }

        $(".solution-btn").on("click", function () {
            var platform = $(this).attr("data-platform");
            $(".platform-showcase-cnt").each(function () {
                if ($(this).attr("data-platform") === platform) {
                    $(this).removeClass("in-active");
                } else {
                    $(this).addClass("in-active");
                }
            });
            $("#showcase .headers h2").each(function () {
                if ($(this).attr("data-platform") === platform) {
                    $(this).addClass("active");
                } else {
                    $(this).removeClass("active");
                }
            });
            $(".other-platforms-card").each(function () {
                if($(this).attr("data-platform") === platform){
                    $(this).addClass("hidden");
                } else {
                    $(this).removeClass("hidden");
                }
            });
            scrollToAnchor("#showcase-anchor");
            $("#showcase .text-container").each(function () {
                $(this).find(".text-group").each(function (i) {
                    if(i === 0){
                        $(this).addClass("active");
                    }else{
                        $(this).removeClass("active");
                    }
                })
            });
        });

        // Solution/Platform text triggers
        let triggerCount = 0;
        $("#showcase .text-container").each(function (i) {
            if ($(this).find(".text-group").length > triggerCount) {
                triggerCount = $(this).find(".text-group").length;
            }
        });

        for (let i = 0; i < (triggerCount - 1); i++) {
            let index = i;
            let trigger = document.createElement("div");
            let offset = 100 / triggerCount;
            let id = "trigger-" + (i + 1);
            trigger.id = id;
            trigger.className = "trigger";
            trigger.style.cssText = "top: " + (offset * (i + 1)) + "%";
            document.getElementById("text-triggers").appendChild(trigger);
            let triggerWaypointDown = new Waypoint({
                element: $("#" + id)[0],
                handler: function (direction) {
                    if (direction === "down") {
                        $("#showcase .text-container").each(function () {
                            $(this).find(".text-group").each(function (i) {
                                if(i === (index + 1)){
                                    $(this).addClass("active");
                                }else{
                                    $(this).removeClass("active");
                                }
                            })
                        });
                    }
                }
            });
            let triggerWaypointUp = new Waypoint({
                element: $("#" + id)[0],
                handler: function (direction) {
                    if (direction === "up") {
                        $("#showcase .text-container").each(function () {
                            $(this).find(".text-group").each(function (i) {
                                if(i === index){
                                    $(this).addClass("active");
                                }else{
                                    $(this).removeClass("active");
                                }
                            })
                        });
                    }
                }
            })
        }

        $("#text-triggers").css("height", $('.platform-showcase-cnt img').height());
    },
};
