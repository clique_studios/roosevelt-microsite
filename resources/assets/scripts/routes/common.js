export default {
  init() {
  	// scroll events
		let windowREF = $(window);
		let scrollTop;
		let windowHeight;

		$(window).scroll(function() {
			let scrollTop = windowREF.scrollTop();
			let windowHeight = scrollTop + windowREF.innerHeight();

		  // animated class
		  $(".animated").each(function() {
		  	let elemTop = $(this).offset().top;

		  	if (windowHeight - 100 > elemTop) {
			  	$(this).addClass('fadeInUp');
			  }
		  });
		});

		$(document).ready(function() {
	    // animated class
		  $(".animated").each(function() {
		  	let elemTop = $(this).offset().top;

		  	if (windowREF.innerHeight() > elemTop) {
			  	$(this).addClass('fadeInUp');
			  }
		  });
		});
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
